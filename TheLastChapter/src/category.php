<?php


class Category{


    public $conn;
    public function __construct()
    {

        $servername = "localhost";
        $username = "root";
        $password = "tamim";
        $dbname = "ecommerce";
        
        try { 
          $this->conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
          // set the PDO error mode to exception
          $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          //  echo "Connected successfully";
 
        } 
        catch(PDOException $e) {
          echo "Connection failed: " . $e->getMessage();


         
        }
    }

    public function insert($data)
    {



      try{

     
      $title=$data['title'];
      $description=$data['description'];

      $filename=$_FILES['picture']['name'];
      $tempname=$_FILES['picture']['tmp_name'];
      $explode_array=explode('.',$filename);
      $file_extension=end($explode_array);
      $imagetype=['png','jpg','gif'];
      $uniqueImageNmae= time().$filename;
      if(in_array($file_extension, $imagetype))
      {
         move_uploaded_file($tempname, '../../assets/images/'.$uniqueImageNmae);

        $query="insert into products (title ,description, picture) values(:product_title,:product_description,:product_picture);";
        $stmt= $this->conn->prepare($query);
        
        $stmt->execute(['product_title'=>$title, 'product_description'=>$description, 'product_picture'=>$uniqueImageNmae]);

       echo "data insert siccessfully";

       header("location: ../categories/home.php");

        header("location: home.php");

      }

      else
      {
        session_start();
        $_SESSION['error']= 'invalid image type  '.'only  '. implode('.',$imagetype).'  are allowed';

        header('location:create.php');
        // echo 'file mush be png or jpg of gif';
      }




      // $query="insert into products (title ,description) values(:product_title,:product_description);";
      //   $stmt= $this->conn->prepare($query);
        
      //   $stmt->execute(['product_title'=>$title, 'product_description'=>$description]);

      //  echo "data insert siccessfully";

      //  header("location: ../categories/home.php");

      //   header("location: home.php");
        
        }
        catch(PDOException $e){
              echo $e->getMessage();
       
        }
        
    }



    public function updatedata($data)
    {



      try{

      $id=$data['id'];
      $title=$data['title'];
      $description=$data['description'];

      $query="update products set title=:product_title,description=:product_description where id=:product_id";
        $stmt= $this->conn->prepare($query);
        
        $stmt->execute(['product_title'=>$title, 'product_description'=>$description,'product_id'=>$id]);

       echo "data insert siccessfully";

       header("location: ../categories/home.php");

        // header("location: home.php");
        
        }
        catch(PDOException $e){
              echo $e->getMessage();
       
        }
        
    }


  public function showdata(){

    try{ 
      $query="select * from  products";
      $stmt= $this->conn->prepare($query);
      $stmt->execute();
      $data = $stmt->fetchAll();
      //  echo '<pre>';
      // print_r($data);
      return $data;
        
    }
    catch(PDOException $e){
          echo $e->getMessage();
   
    }

  
  
  }



    public function show($id){

      try{      
      $query="select title,description,picture from  products where id= :product_id";
      $stmt= $this->conn->prepare($query);
      $stmt->execute(['product_id'=>$id]);

  


      $data = $stmt->fetch();

      return $data;      
      }

      catch(PDOException $e){
        echo $e->getMessage();
 
      }
  
 


    }


    public function editdata($id){

      try{      
        $query="select title,description from  products where id= :product_id";
        $stmt= $this->conn->prepare($query);
        $stmt->execute(['product_id'=>$id]);
  
    
  
  
        $data = $stmt->fetch();
  
        return $data;      
        }
      catch(PDOException $e){
            echo $e->getMessage();
     
      }
  
    
    
    }

    public function delete($id){

       try{      

      $query="delete  from  products where id=:product_id";
      $stmt= $this->conn->prepare($query);
      $stmt->execute(['product_id'=>$id]);


      header('location:../categories/home.php');

      // $data = $stmt->fetch();
      // //  echo '<pre>';
      // // print_r($data);

      // return $data;
       }
       catch(PDOException $e){
        echo $e->getMessage();
 
       }



    }








}
